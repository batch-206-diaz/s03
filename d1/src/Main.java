import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //System.out.println("Hello world!");

    /*

        Loops
            - control structures that allow code blocks to be repeated according to the conditions set

            - Type:
                - While - run codes repeatedly based on a condition
                - Do-While - will run "Do" at least once even if it's false
                - For - more versatile and more commonly used
                _ Enhanced For - (for-each) used to loop over EACH item in arrays and arrays list

        Multidimensional Arrays
            that are nested within each other. First array could be for the rows, the second could be for columns
            - Syntax:
                data_type[][] array_name = new data_type[row_size][column_size];

        Exception Handling
            - refers to managing and catching run-time errors
            - "exceptions" are unexpected errors that happen after the program has been compiled and run
            - important to not let our program end abnormally because of a run-time error.
            - try-catch-finally statement is used to catch exceptions


    */

        Scanner userInput = new Scanner(System.in);

/* WHILE LOOP */

        int a = 1;
        while(a < 5){
            System.out.println("While Loop Counter: " + a);
            a++;
        }

        // ***
        boolean hasNoInput = true;
        while(hasNoInput){

            System.out.println("Enter your name:");
            String name = userInput.nextLine();

            if(name.isEmpty()){
                System.out.println("Please try again.");
            } else {
                hasNoInput = false;
                System.out.println("Thank you for your input!");
            }

        }

/* DO-WHILE LOOP */

        int b = 5;
        do {
            System.out.println("Countdown: " + b);
            b--;
        } while (b > 1);

/* FOR LOOP */

        /* SIMPLE FOR LOOP */

        for(int i = 1; i <= 10; i++){
            System.out.println("Count: " + i);
        }

        /* FOR LOOP OVER AN ARRAY */

        int[] intArray = {100,200,300,400,500};
        for(int i = 0; i < intArray.length; i++){
            System.out.println("Item at index number " + i + " is " + intArray[i]);
        }

        /* LOOP OVER A MULTIDIMENSIONAL ARRAY */

        String[][] classroom = new String[3][3];

        //first Row
        classroom[0][0] = "Rayquaza";
        classroom[0][1] = "Kyogre";
        classroom[0][2] = "Groudon";

        //second Row
        classroom[1][0] = "Sora";
        classroom[1][1] = "Goofy";
        classroom[1][2] = "Donald";

        //third Row
        classroom[2][0] = "Harry";
        classroom[2][1] = "Ron";
        classroom[2][2] = "Hermione";

        System.out.println(Arrays.toString(classroom));
        System.out.println(Arrays.deepToString(classroom));

        //looping over each and every item in the classroom array
        for(int row = 0; row < 3; row++){
            for (int col = 0; col < 3; col++){
                System.out.println(classroom[row][col]);
            }
        }

/* ENHANCED FOR LOOP */

        String[] members = {"Eugene","Vincent","Dennis","Alfred"};
        for(String member: members){
            System.out.println(member);
        }

        for(String[] arr: classroom){
            System.out.println(Arrays.toString(arr));
            for(String name: arr){
                System.out.println(name);
            }
        }

        HashMap<String,String> techniques = new HashMap<>();
        techniques.put(members[0],"Spirit Gun");
        techniques.put(members[1],"Black Dragon");
        techniques.put(members[2],"Rose Whip");
        techniques.put(members[3],"Spirit Sword");
        System.out.println(techniques);

        techniques.forEach((key,value) -> {
            System.out.println("Member " + key + " uses " + value);
        });

        //int numberSample = "25000";


/* EXCEPTION HANDLING */

        System.out.println("Enter an integer:");
        //int num = userInput.nextInt();
        //System.out.println(num);

        int num = 0;
        try{
            num = userInput.nextInt();
        } catch (Exception e){
            System.out.println("Invalid Input"); // -- prints a message if there's an exception
            e.printStackTrace(); // -- prints the error
        }

        System.out.println("Hello from the other side!");

    }
}