import java.util.*;

public class Main {
    public static void main(String[] args) {
        //System.out.println("Hello world!");

// Create Array for game titles */
//        String[] games = {
//                "Mario Odyssey",
//                "Super Smash Bros. Ultimate",
//                "Luigi's Mansion 3",
//                "Pokemon Sword",
//                "Pokemon Shield"
//        };

/* Create Hashmap with 'games' array and input values for 'stocks' */
        HashMap<String,Integer> gameStocks = new HashMap<>();
//        stocks.put(games[0],50);
//        stocks.put(games[1],20);
//        stocks.put(games[2],15);
//        stocks.put(games[3],30);
//        stocks.put(games[4],100);
        gameStocks.put("Mario Odyssey",50);
        gameStocks.put("Super Smash Bros. Ultimate",20);
        gameStocks.put("Luigi's Mansion 3",15);
        gameStocks.put("Pokemon Sword",30);
        gameStocks.put("Pokemon Shield",100);

/* Create Array List for top games */
        ArrayList<String> topGames = new ArrayList<>();

        //Print each game with their stocks number
        gameStocks.forEach((key,value) -> {
            System.out.println(key + " has " + value + " stocks left.");

            //while going over each item, check stock to populate topGames
            if(value <= 30){
                topGames.add(key);
            }
        });

/* Print Contents of topGames */
        for(String game: topGames){
            System.out.println(game + " has been added to top game list!");
        }

        System.out.println("Our shop's top games:");
        System.out.println(topGames);


/* Stretch Goal: Add item */

        Scanner itemScanner = new Scanner(System.in);

        boolean addItem = true;


        while(addItem){
            System.out.println("Would you like to add an item? Yes or No.");
            String input = itemScanner.nextLine();

            switch(input){

                case "Yes":
                    //Read User's Inputs
                    System.out.println("Add the item name:");
                    String itemName = itemScanner.nextLine();
                    System.out.println("Add the item stock:");
                    int itemStock = itemScanner.nextInt();

                    /* Insert Input Data to the Records */
                    //ArrayList<String> gamesList = new ArrayList<>(Arrays.asList(games));
                    //gamesList.add(itemName);
                    //stocks.put(gamesList.get(gamesList.size()-1),itemStock);
                    gameStocks.put(itemName,itemStock);

                    // Successful Update
                    System.out.println("You have successfully added " + itemName + " with " + gameStocks.get(itemName) + " stock/s.");
                    addItem = false;
                    break;

                case "No":
                    System.out.println("Thank you.");
                    addItem = false;
                    break;

                default:
                    System.out.println("Invalid.");
            }
        }

    }
}















